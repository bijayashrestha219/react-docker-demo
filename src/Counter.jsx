import React, {useState} from 'react';

function Counter({initialCount}){
       const [count, setCount] = useState(initialCount);

        const increment = () => {
            setCount((prev) => prev + 1);
        }

        const decrement = () => {
            setCount((prev) => prev - 1);
        }

        const restart = () => {
            setCount(0);
        }

        const switchSign = () => {
            setCount((prev) => prev * -1);
        }

       return (
        <>
            <div>
                Counter: <span data-testid="count">{count}</span>
            </div>
            <button onClick={increment}>Increment</button>
            <button onClick={decrement}>Decrement</button>
            <button onClick={restart}>Restart</button>
            <button onClick={switchSign}>Switch</button>
        </>
       )
}

export default Counter;