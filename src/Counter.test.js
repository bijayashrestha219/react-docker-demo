import { fireEvent, render } from "@testing-library/react";
import Counter from "./Counter";

describe(Counter, () => {
    it("counter displays initial count", () => {
        const {getByTestId} = render(<Counter initialCount={0}/>);
        const countValue = Number(getByTestId("count").textContent);
        expect(countValue).toEqual(0);
    });

    it("counter should increment by 1 if the Increment button is clicked", () => {
        const {getByTestId, getByRole} = render(<Counter initialCount={0}/>);
        const incrementButton = getByRole("button", {name: "Increment"});
        const countValue1 = Number(getByTestId("count").textContent);
        expect(countValue1).toEqual(0);
        fireEvent.click(incrementButton);
        const countValue2 = Number(getByTestId("count").textContent);
        expect(countValue2).toEqual(1);
    });

    it("counter should decrement by 1 if Decrement button is clicked", () => {
        const {getByTestId, getByRole} = render(<Counter initialCount={0}/>);
        const decrementButton = getByRole("button", {name: "Decrement"});
        const countValue1 = Number(getByTestId("count").textContent);
        expect(countValue1).toEqual(0);
        fireEvent.click(decrementButton);
        const countValue2 = Number(getByTestId("count").textContent);
        expect(countValue2).toEqual(-1);

    })

    it("counter should set to 0 if Restart button is clicked", () => {
        const {getByTestId, getByRole} = render(<Counter initialCount={5}/>);
        const restartButton = getByRole("button", {name: "Restart"});
        const countValue1 = Number(getByTestId("count").textContent);
        expect(countValue1).toEqual(5);
        fireEvent.click(restartButton);
        const countValue2 = Number(getByTestId("count").textContent);
        expect(countValue2).toEqual(0);

    })

    it("counter should change to negative number if Switch button is clicked", () => {
        const {getByTestId, getByRole} = render(<Counter initialCount={5}/>);
        const switchButton = getByRole("button", {name: "Switch"});
        const countValue1 = Number(getByTestId("count").textContent);
        expect(countValue1).toEqual(5);
        fireEvent.click(switchButton);
        const countValue2 = Number(getByTestId("count").textContent);
        expect(countValue2).toEqual(-5);

    })


}) 