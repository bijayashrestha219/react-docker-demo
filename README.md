# React Boilerplate

`Divio Cloud <http://www.divio.com/>`_ based boilerplate to develop with React.

The sample project was created using the
`Create React App <https://facebook.github.io/create-react-app/docs/getting-started>`_.

Up to date with `React <https://reactjs.org//>`_ **18.2.0**.

## Getting started

### Setup local dev environment for first time

After setting up a project you are required to run `docker-compose run --rm web npm install` 
in order for node modules to appear in the mounted volume (which means, installing all node package dependencies).


> Note: Yarn can also be used instead of NPM. If you choose to use Yarn instead of NPM you can simply run ``yarn install`. It creates yarn.lock file which is equivalent to `package-lock.json`.

### Run project
```bash
# Start the container
docker-compose up -d 

# Stop the containers
docker-compose down

# To SSH into the container
docker exec -ti react-docker-web bash

# To run npm commands from host using docker cli
docker exec -ti react-docker-web npm test
docker exec -ti react-docker-web npm start
docker exec -ti react-docker-web npm build

docker exec -ti react-docker-web node -v

```